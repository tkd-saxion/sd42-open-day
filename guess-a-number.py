# Guess a number
# You have to guess a random picked secret number in 10 turns.
# The game gives only 3 answers until all turns are over:
# * Higher - The number you guessed is higher then the secret number.
# * Lower - The number you guessed is lower then the secret number.
# * Winner - You guessed the secretly picked number.

import random
#TODO set a couple of constants

#TODO Welcome to the game with amount of guesses

#TODO Generate a secret_number with the constants

#TODO Print in what range the secret number will be

#TODO Loop thru the turns until the maximum turns constant

    #TODO Ask for a guess
    #TODO Check if the user has input something or the input is not a number
    #TODO If the guess is lower then the secret number, give feedback to user.
    #TODO If the guess is higher then the secret number, give feedback to user.
    #TODO If the guess is the secret number, Game ends with the answer and the amount of turns used.
    
#TODO When the user used all turns, end the game and show the secret number