# Caesar cipher (https://en.wikipedia.org/wiki/Caesar_cipher)
# It is one of of the simplest and most widely known encryption techniques. 
# It is a type of substitution cipher in which each letter in the plaintext is 
# replaced by a letter some fixed number of positions down the alphabet. 
#
# In this assignment we are only working with capitalized characters

#TODO Implement the ceaser cipher encode and decode

def is_space(char: chr) -> bool:
    pass

def encode_ceaser(phrase : str):
    pass

def decode_ceaser(secret: str):
    pass

#Test encode and decode
print(encode_ceaser("YOU ARE AWESOME"))
print(decode_ceaser("ZPV BSF BXFTPNF"))

#TODO Solve the hardest one with a shift
print(decode_ceaser("TJHO VQ GPS B USJBM TUVEZ EBZ BU UIF BTTPDJBUF EFHSFF TPGUXBSF EFWFMPQNFOU", 21))