# Saxion - AD-SD - Open Day
When you study at the Assoicate Degree Software Development you can follow each day at 15:30 an hour long coding session. During a coding session, a group of up to fifteen students and one teacher will gather around a screen to code something together. The teacher will be at the keyboard. Students are expected to come up with ideas on what to implement, and will be asked to propose and discuss implementation techniques.

At the "open-day" from the study there will be a short coding session of 20 minutes. Here you get the change to get a experience what you can expect during your study period.

The examples are written on the level of the first 4 weeks of the study.

## Authors
[Timothy Sealy](https://gitlab.com/sealy)  
[Tibor Kleinman Derksen](https://gitlab.com/tkd-saxion)

## How to start
The project is seperated in three branches
* main - The assignment with description without any code, good luck!
* halfway - The assignments that are 50-percent done.
* solution - The assignments that are fully solved

## Examples
The code-examples that are used in the short code-session:
* Guess a number (Higher or Lower)
* Ceaser-Cipher (Encode and Decode a message)

## TODO 
* More examples